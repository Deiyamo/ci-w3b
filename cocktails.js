import fs from "fs";

let cocktails = [];

/**
 * CHARGE LES COCKTAILS DU FICHIER DANS LE TABLEAU
 */
const loadCocktails = () => {
    try {
        const data = fs.readFileSync("./cocktails.json", "utf8");
        cocktails = JSON.parse(data);
    } catch (err) {
        console.error(`loadCocktails > ${err}`);
    }
};

/**
 * ECRIT LE TABLEAU DE COCKTAILS DANS LE FICHIER
 */
const writeCocktails = () => {
    try {
        fs.writeFileSync("./cocktails.json", JSON.stringify(cocktails, null, 2));
    } catch (err) {
        console.error(`writeCocktails > ${err}`);
    }
};

/**
 *  ID, NOM, PRICE, ALCOOL, INGREDIENTS (chaines de caractères séparation avec un |), DESCRIPTION
 */
const add = (id, name, price, alcohol, ingredients, description) => {
    loadCocktails();
    const pdt = cocktails.find((e) => e.name === name);

    cocktails.push({ id, name, price, alcohol, ingredients, description });

    writeCocktails();

    console.log(`${name} ajouté`);
};

/**
 * RETOURNE LA LISTE DES COCKTAILS
 * @returns [object]
 */
const getAll = () => {
    loadCocktails();
    return cocktails;
};

/**
 * RETOURNE UN COCKTAIL PAR SON NOM
 * @param {*} id
 * @returns object : cocktails
 */
const getByName = (name) => {
    loadCocktails();
    return cocktails.find((p) => p.name === name);
};

/**
 * MET A JOUR UN COCKTAILS PAR SON NOM
 * @param {string} name
 * @param {object} p : cocktails
 * @returns boolean
 */
const update = (name, p) => {
    loadCocktails();
    const i = cocktails.findIndex((e) => e.name === name);

    if (i > -1) {
        cocktails[i] = p;
        writeCocktails();
        return true;
    }

    return false;
};

/**
 * SUPPRIME LE COCKTAIL SI AUCUNE QUANTITY PASSEE SINON SUPPRIME LE NOMBRE DE QUANTITY
 * @param {*} name
 * @param {*} quantity
 * @returns
 */
const remove = (name, quantity) => {
    const pdt = cocktails.find((e) => e.name === name);

    if (pdt) {
        if (quantity) {
            // pas assez de stock
            if (quantity > pdt.quantity) {
                console.log(`Stock insufisant pour ${name} : ${pdt.quantity} max`);
                return false;
            }

            // soustrait le stock
            pdt.quantity -= quantity;
            console.log(`${quantity} ${name} supprimé(e)(s), reste ${pdt.quantity}`);
            writeCocktails();
            return true;
        }

        // supprime le produit
        cocktails = cocktails.filter((e) => e.name !== name);
        console.log(`${cocktails.name} supprimé(e)s, 0 stock`);
        writeCocktails();
        return true;
    }

    return false;
};

export { add, getAll, getByName, update, remove };
