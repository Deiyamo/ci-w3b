import {router} from './routes.js';
import express from 'express';
import {config} from 'dotenv';

config()
const app = express()
app.get('/ping', (req, res) => res.send("pong"))
app.listen(process.env.PORT, () => console.log(`Listening on ${process.env.PORT}...`))
app.use(router);
