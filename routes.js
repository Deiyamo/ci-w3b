import express from 'express'
import {add, getAll, remove} from "./cocktails.js";
const router = express.Router();

// GET : req.params.name
// POST : res.body
// res.status(401).send( obj ); // précise le statut

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

// display all cocktails
router.get('/cocktails', (req, res) => {
    res.send( getAll() );
});

// add one product
// id, name, price, alcohol, ingredients, description
router.post('/cocktails', (req, res) => {
    add(req.body.id, req.body.name, req.body.price, req.body.alcohol, req.body.ingredients, req.body.description)
    res.send(req.body.name + ' has been added.');
});

// display one product
router.get('/cocktails/:name', (req, res) => {
    let id = req.params.name;
    let obj = { id : id, Content : "content " +id };

    res.send( obj );
});


// delete one product
router.delete('/cocktails/:name', (req, res) => {
    remove(req.body.name)
    res.send(`${req.body.name} has been deleted`);
})

export {router};
