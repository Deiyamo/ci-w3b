import {getAll} from "../server/products.mjs";
import p from "../server/product.json"


//test("Products", () => expect(getAll()).toMatchObject([{"name": "bob", "quantity": 11}, {"name": "jean", "quantity": 2}, {"name": "local", "quantity": 22}]))
test("Products json", () => expect(p).toMatchObject([{"name": "bob", "quantity": 11}, {"name": "jean", "quantity": 2}, {"name": "local", "quantity": 22}]));
