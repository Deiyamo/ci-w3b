import {Add, Subtract, Multiply} from "./cocktails.js"

const a = 11;
const b = 2;

describe("Must do 3 operations", () => {
    test("Addition", () => expect(Add(a, b)).toBe(13));
    test("Subtraction", () => expect(Subtract(a, b)).toBe(9));
    test("Multiplication", () => expect(Multiply(a, b)).toBe(22));
});